import React from 'react';
import Button from './button';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom/';

test('check text is equal to KILLIAN', () => {
  const { queryByText } = render(<Button text="KILLIAN" />);
  
  expect(queryByText("KILLIAN")).toBeInTheDocument(); 
});