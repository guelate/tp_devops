# Utilisation d'une image Node.js pour la construction de l'application
FROM node:18-alpine AS builder

# Définir le répertoire de travail dans le conteneur
WORKDIR /app

# Copier les fichiers nécessaires pour l'étape de build
COPY tp/package*.json ./
COPY tp/. .

RUN npm install
RUN npm run build

FROM node:18-alpine

WORKDIR /app

COPY --from=builder /app/. .

EXPOSE 3000

CMD ["npm", "run", "dev"]

